import React, { Component } from 'react'
import Header from './components/Header'
import RouterComponent from './components/RouterComponent'
import { Row, Container, Col } from 'react-bootstrap'
import Sidenavbar,{history as customHistory} from "./components/Sidenavbar"
import styled from 'styled-components'
import {Provider} from 'mobx-react'
import stores from './stores';
import { Router,Route, Switch, Redirect} from 'react-router';
import Login from './containers/Login';
import UserMenu from './containers/UserMenu';
import "./App.scss";

const Main = styled.main`
    position: relative;
    overflow: hidden;
    height:100vh;
    transition: all .15s;
    padding:0px 0px 0px 20px;
    margin-left: ${props=>props.about? 240-20:64-20}px;
`;


interface IProps{}
interface IState{
    expanded?: string
}

export default class App extends Component<IProps,IState> {
    constructor(props: IProps) {
        super(props);
        this.state ={
            expanded:""
        }
    }
  
  render() {
    return (
        <Provider {...stores}>
            <Router history={customHistory} >
                    <Switch>
                        <Route path="/login" exact={true} component={ OutContainer } />
                        <LoggedContainer />
                        <Route path='*' component={Login} />
                    </Switch>
            </Router> 
        </Provider>
    );
  }
}

class LoggedContainer  extends Component<IProps,IState> {
    constructor(props: IProps) {
        super(props);
        this.state ={
            expanded:""
        }
    }

    onToggle = (expanded:string) => {
        this.setState({ expanded: expanded });
    };

    render(){
        return(
            
            <div id='app'>
                     {
                  !stores.authStore.logged || !window.localStorage.getItem('jwt') ?
                    <Redirect to={'login'}/>
                  :
                   null
                }
                <Sidenavbar  {...this.props} onToggle={this.onToggle} isOpened={this.state.expanded}/>
                <Main about={this.state.expanded}>
                    <Header><UserMenu/></Header>
                    <Container fluid className="fill">
                            <Row className="fillHeight">
                                <Col md={12}>
                                    <RouterComponent />
                                </Col>
                            </Row>
                    </Container>
                </Main>
            </div>
            );
        }

}


const OutContainer = (props: {children?: any}) =>
<div className="page-container">
    <Header/>
    <Container fluid>
        <Row  className="justify-content-md-center">
            <Login/>
        </Row>
    </Container>
</div>;


