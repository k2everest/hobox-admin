import React, { Component } from 'react'
import { Navbar } from 'react-bootstrap'
import "./style.scss";

interface IProps {
    children?: any
}
export default class Header extends Component<IProps,any> {

    render(){
        return (
            <nav className="navbar navbar bg-light navbar-light">
                <Navbar.Brand href="/">Painel</Navbar.Brand>
                {this.props.children}
            </nav>
        )
    }
}
