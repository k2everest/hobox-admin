import React, { Component } from 'react'
import {Route} from 'react-router'
import Footer from '../Footer';
import UserList from  '../../containers/User/List';
import Financial from  '../../containers/Financial';


export default class RouterComponent extends Component{
    
    render(){
        return (
            <div>   
                    <Route path="/" exact={true} component={Financial} />
                    <Route path="/financial"  component={Financial} />
                    <Route path="/users"   exact={true} component={UserList} />
                    <Route path="/checkout" component={Footer} />
            </div>

        )
    }
}
