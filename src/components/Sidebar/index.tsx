import React,{Component} from "react";
import { slide as Menu } from "react-burger-menu";

interface IProps{
    pageWrapId: any,
    outerContainerId: any
}
export default class Sidebar extends Component<IProps,any>{

    constructor(props: IProps, context: any) {
        super(props, context);

    }

    render(){
        return (
            // Pass on our props
            <Menu {...this.props}>
            <a className="menu-item" href="/">
                Home
            </a>

            <a className="menu-item" href="/burgers">
                Burgers
            </a>

            <a className="menu-item" href="/pizzas">
                Pizzas
            </a>

            <a className="menu-item" href="/desserts">
                Desserts
            </a>
            </Menu>
        );
    }
};