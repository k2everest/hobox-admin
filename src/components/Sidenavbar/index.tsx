import React,{Component} from 'react'
//@ts-ignore
import SideNav, { NavItem, NavIcon, NavText, Nav } from '@trendmicro/react-sidenav';
import { FaChartLine,FaUsers } from 'react-icons/fa';
import { createBrowserHistory } from 'history';
import '@trendmicro/react-sidenav/dist/react-sidenav.css';
import styled from 'styled-components'
const history = createBrowserHistory({
 basename: '/admin/'
});

export {history};


const s = require('./style.scss');
interface IProps{
    onToggle: ({})
    isOpened?: string,
}
interface IState{
    currentPath?:string;
}

const NavHeader = styled.div`
    display: ${props => (props.about ? 'flex' : 'none')};
    white-space: nowrap;
    background-color: #db3d44;
    color: #fff;
    > * {
        color: inherit;
        background-color: inherit;
    }
    justify-content: left;
    flex-direction: column;
`;

// height: 20px + 10px + 10px = 40px
const NavTitle = styled.div`
    font-size: 1.5em;
    line-height: 20px;
    padding: 10px 0;
    margin: 5px 20px;
`;

// height: 20px + 4px = 24px;
const NavSubTitle = styled.div`
    font-size: 1em;
    line-height: 20px;
    padding-bottom: 4px;
    margin: 0px 20px;
`;


export default class Sidenavbar extends Component<IProps,IState> {
    constructor(props: IProps) {
        super(props);
        const path = history.location.pathname.replace('/','');
        this.state ={
            currentPath: path.length > 0 ? path : 'financial'
        }

    }

    render(){
        return(
            <SideNav
                onSelect={(selected: any) => {
                    console.log("/"+selected);
                    history.push("/"+selected);
                }}
                onToggle={this.props.onToggle}
            >
            <NavHeader about={this.props.isOpened}>
                            <NavTitle>Hobox</NavTitle>
                            <NavSubTitle>Administrador</NavSubTitle>
                        </NavHeader>
            <SideNav.Toggle />
            <Nav defaultSelected={this.state.currentPath} >
            <NavItem eventKey={'financial'}  >
                <NavIcon> 
                    <FaChartLine />
                </NavIcon>
                    <NavText>
                        Financeiro
                    </NavText>
                </NavItem>
                <NavItem eventKey="users">
                    <NavIcon>
                        <FaUsers/>                
                    </NavIcon>
                    <NavText>
                        Usuários
                    </NavText>
                </NavItem>
            </Nav>
        </SideNav>
        )
      } 
    
}