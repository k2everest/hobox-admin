const API = {
    BASE_URL : process.env.NODE_ENV=='development'? "http://localhost:3000": "https://app.hobox.com.br:8080"
}

export { API }