import * as React from 'react';
import {observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import { observable } from 'mobx';
import {Jumbotron, Container, Row, Col} from 'react-bootstrap';

interface Props {
}

@observer
export default class Financial extends React.Component<Props, any> {
    @observable error: string = '';

    constructor(props: Props, context: any) {
        super(props, context);
    }

    render() {
        return (
          <div>
                <h1>Financeiro</h1>
                <Container fluid>
                  <Row className="justify-content-md-center">
                    <Col>
                    <Jumbotron>
                        <h1>Entrada Mensal</h1>
                        <p>
                        R$0.00
                        </p>
                    </Jumbotron>
                    </Col>
                    <Col>

                    <Jumbotron>
                        <h1>Entrada Total</h1>
                        <p>
                        R$0.00
                        </p>
                    </Jumbotron>
                </Col>
                </Row>
                </Container>


                
        </div>);
    }
}
