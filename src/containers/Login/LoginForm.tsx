import * as React from 'react';
import { observable } from 'mobx';
import { observer } from 'mobx-react';
import { Button } from 'react-bootstrap';
//import { Link } from 'react-router-dom';
import stores  from '../../stores/';

/** Style*/
import './style.scss';

export interface LoginFormFields {
    user: string;
    password: string;
}

interface ILoginFormProps {
    onSubmit: (fields: LoginFormFields) => any;
};

interface ILoginFormState {};

@observer
export default class LoginForm extends React.Component<ILoginFormProps, ILoginFormState> {
    @observable user: string = '';
    @observable password: string = '';

    componentWillUnmount(){
      stores.authStore.errors = undefined;
    }

    onSubmit = (e: React.FormEvent<HTMLFormElement>) => {
        e.preventDefault();
        this.props.onSubmit({
          user: this.user,
          password: this.password
        });

    }
    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit.bind(this)} className={"form"}>
                  <div className={"field"}>
                        <input type="text"
                            placeholder="E-mail"
                            value={this.user}
                            onChange={ e => this.user = e.target.value }
                        />
                    </div>

                    <div className={"field"}>
                        <input type="password"
                            placeholder="Senha"
                            value={this.password}
                            onChange={ e => this.password = e.target.value }
                        />
                    </div>
                    <Button type="submit">Entrar</Button>
                    <div className={"links"}>
                    {/*   <Link to={`/user/forgot-password`} >Esqueci minha senha</Link>
                      <Link to={`/signup`} >Inscreva-se</Link> */}
                    </div>
                </form>
            </div>
        );
    }
};
