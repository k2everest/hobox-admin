import * as React from 'react';
import { observer } from 'mobx-react';
import { observable } from 'mobx';
import stores  from '../../stores/';
import   {Redirect} from 'react-router';
import { Modal } from 'react-bootstrap';

/**
 * Components
 */
import LoginForm, {LoginFormFields} from './LoginForm';


/* Style */
import  './style.scss';

interface ILoginProps {};

interface ILoginState {};

@observer
export default class Login extends React.Component<ILoginProps, ILoginState> {
    @observable error: string = '';
    authStore = stores.authStore;

    componentWillMount(){
      this.authStore.logged = false;
    }

    onSubmit = (fields: LoginFormFields) => {
        this.authStore.setEmail(fields.user);
        this.authStore.setPassword(fields.password);
        this.authStore.login();
    }

    render() {
        return (
            <div className="col-md-5  col-lg-5 col-md-offset-5 col-lg-offset-5">
            { this.authStore.inProgress === true ?
                    <Modal show={true} container={this} onHide={()=>{}}>
                        <div className={"spinner"} />
                    </Modal>
                    : null
                }

                        <div className={"form"}>
                            <h1>Login</h1>

                            {(this.authStore.logged ?
                                    <Redirect to={'/'}/>
                                :
                                    <LoginForm onSubmit={this.onSubmit}/>
                            )}
                            {(this.authStore.errors === undefined ? null : (
                                this.authStore.errors==='Not allowed'?
                                    <div className={'error'}>Acesso Negado </div>
                                :
                                <div className={'error'}> Usuário ou senha incorretos</div>
                            ))}
                        </div>
            </div>
        );
    }
};
