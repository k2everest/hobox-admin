import * as React from 'react';
import {observer} from 'mobx-react';
import {Link} from 'react-router-dom';
import { observable } from 'mobx';
//import {linkToUserDetails} from '../Route/RouteStore';
import { userStore }  from '../../../stores/rootStore';
//const Spinner = require('react-spinkit');
//const s =  require('./style.scss');
import { DropdownButton,Dropdown } from 'react-bootstrap';

interface Props {
}

@observer
export default class UserList extends React.Component<Props, any> {
    @observable error: string = '';
    userStore = userStore;

    constructor(props: Props, context: any) {
        super(props, context);
    }

    componentWillMount(){
        this.userStore.listUsers();
    }

    render() {
        return (
          <div>
                <h1>Usuários</h1>
                {this.userStore.inProgress === true ?
                    <label>carregando...</label> :
                    <table className="table">
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>E-mail</th>
                            <th/>
                        </tr>
                        </thead>
                        <tbody>

                        {this.userStore.users.map(u =>
                            <tr key={u._id}>
                                <td>{`${u.name} ${u.lastname}`}</td>
                                <td>{u.email}</td>
                                <td>
                                    <DropdownButton
                                        alignRight
                                        title=""
                                        id="dropdown-menu-align-right"
                                        >
                                        <Dropdown.Item eventKey="1">Desativar Conta</Dropdown.Item>
                                    </DropdownButton>          
                                </td>
                            </tr>
                        )}

                        </tbody>
                    </table>
                }
        </div>);
    }
}
