import * as React from 'react';
import { Dropdown } from 'react-bootstrap';
import {authStore} from '../../stores/rootStore';
import { FaUser } from 'react-icons/fa';
import './style.scss';

export default class  UserMenu extends React.Component<{}, {}>{

    render(){
        return(
            !authStore.logged?
                null
            :
            <div >
                <Dropdown id='userMenu' alignRight >
                <Dropdown.Toggle  id="dropdownUser">
                    <FaUser />
                </Dropdown.Toggle>

                <Dropdown.Menu className={"menu"} >
                    <label className="dropdown-header">{authStore.user.username}</label>
                    <Dropdown.Item eventKey="1" onClick={authStore.logout}>Sair</Dropdown.Item>
                </Dropdown.Menu>
                </Dropdown>
            </div>
        );
    }
};
