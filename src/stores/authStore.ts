import { observable, action, reaction } from 'mobx'
import {createBrowserHistory} from 'history'
import api from '../services/api'
import {userStore, authStore} from './rootStore'
const customHistory = createBrowserHistory()



type ResetFields  = {
  newPassword: string;
  confirmPassword: string;
  token: string;
};

class AuthStore {
  @observable inProgress = false;
  @observable errors = undefined;
  @observable user = {
    token : window.localStorage.getItem('jwt'),
    username: window.localStorage.getItem('username'),
    role: window.localStorage.getItem('role')
  };
  @observable logged = !!this.user.token;
  @observable appLoaded = false;
  @observable isLoadingTags = false;
  @observable done = false;
  @observable successMessage ='';
  @observable resetedPassword = false;
  @observable recoverMode = false;
  
  @action setDone() {
    this.inProgress = false;
    this.done =  true;
  
  }
  

  @observable values = {
    username: '',
    email: '',
    password: '',
  };

  constructor() {
    
    reaction(
        () => this.user,
        user => {
          if (user.token && user.username && user.role==='Admin')  {
            window.localStorage.setItem('jwt', user.token);
            window.localStorage.setItem('username', user.username);
            window.localStorage.setItem('role',user.role);
            this.logged = true;
          } else {
            localStorage.clear();
            if(customHistory.location.pathname!='login' && !window.localStorage.getItem('jwt'))
              this.errors = 'Not allowed';
            //window.location.href = 'login';
          }
        }
    );
  }

  @action setUser(user:{token:string, username:string,role: string}) {
    this.user = user;
  }

  @action setAppLoaded() {
    this.appLoaded = true;
  }

  @action setUsername(username) {
    this.values.username = username;
  }

  @action setEmail(email) {
    this.values.email = email;
  }

  @action setPassword(password) {
    this.values.password = password;
  }

  @action reset() {
    this.values.username = '';
    this.values.email = '';
    this.values.password = '';
  }

  @action login() {
    this.inProgress = true;
    this.errors = undefined;
    return api.Auth.login(this.values.email, this.values.password)
      .then(( user ) => this.setUser(user))
      .catch(action((err) => {
        this.inProgress=false;
        this.errors = err;
      }));
  }

  @action logout() {
    let user = {username:'',token:'', role:''}
    authStore.setUser(user);
    userStore.forgetUser();
    return new Promise(res => res());
  }

  @action setRecoverMode() {
    this.recoverMode = true;
    return !window.localStorage.getItem('recoverMode') ? window.location.href = '/' : null;
  }

  @action unsetRecoverMode() {
    window.localStorage.removeItem('recoverMode');
    window.location.href = '/' ;
  }

  @action forgotPassword(){
    this.inProgress = true;
    this.errors = undefined;
    return api.Auth.forgotPassword(this.values.email)
    .then(()=>{
      this.successMessage = 'E-mail enviado. Por favor, verifique seu e-mail';
      this.setDone();
    })
    .catch((err)=>{
      this.errors = err.response.text;
      this.setDone();
    })
  }


  @action setPasswordReset(fields: ResetFields){
    this.inProgress = true;
    this.errors = undefined;
    return api.Auth.resetPassword(fields)
    .then(()=>{
      this.successMessage = 'Senha alterada com sucesso';
      this.setDone();
      this.resetedPassword = true;
    })
    .catch((err)=>{
      this.errors = err.response.body.message;
      this.setDone();
    })
  }


}

export default AuthStore;

