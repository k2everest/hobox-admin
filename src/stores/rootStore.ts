import AuthStore from './authStore';
import UserStore  from './userStore';

const authStore = new AuthStore();
const userStore = new UserStore();



export default { authStore, userStore };
export  { authStore, userStore };

